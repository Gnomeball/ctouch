## ctouch

A one-page solution to creating both a header and implementation file for C and C++.

Essentially I got bored having to make them both manually .. so I wrote this.

### Usage

Stick it somewhere on your path, then call:

`ctouch <c/cpp> <filename>`

And in your current directory will appear, assuming they do not already exist, a `.c/pp` and a `.h/pp` file of that name.

### Examples

Examples for `ctouch c hello` and `ctouch cpp world` can be seen inside the `/examples` directory.
